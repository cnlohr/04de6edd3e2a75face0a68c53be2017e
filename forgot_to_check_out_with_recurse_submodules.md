Did you just check out / clone a repository and forget --recursive / --recurse-submodules?  Well, do this:

```
git submodule update --init --recursive --remote
```

For information about why this nugget was lost and burried in another stack exchange article, see this: https://meta.stackoverflow.com/questions/400424/how-can-i-request-that-a-question-merge-be-reversed